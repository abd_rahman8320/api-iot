<?php
$servername = "202.52.146.100";
$username = "iotsraco_test";
$password = "iotsra20200201";
$dbname = "iotsraco_summary";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//Authentication
if (isset($_GET['token_access'])) {
    $date_now = date('Y-m-d H:i:s');

    $sql_auth = 'SELECT * FROM token_access WHERE token = "'.$_GET['token_access'].'" AND expired_date > "'.$date_now.'"';
    $result_auth = $conn->query($sql_auth);

    if ($result_auth->num_rows == 0) {
        $data = [
            'status' => 2,
            'error' => 'Token Invalid. Please, Restart /start Command'
        ];

        echo json_encode($data);
        die();
    }
}else {
    $data = [
        'status' => 2,
        'error' => 'Token Invalid. Please, Restart /start Command'
    ];

    echo json_encode($data);
    die();
}

if (isset($_GET['q'])) {
    $token = $_GET['q'];
    $date_now = date('Y-m-d H:i:s');

    $sql = 'SELECT * FROM token_download WHERE token = "'.$token.'" AND expired_date > "'.$date_now.'"';
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $file = $row['file'];

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: private');
            header('Pragma: private');
            header('Content-Length: '.filesize($file));
            ob_start();
            ob_clean();
            ob_flush();
            flush();
            readfile($file);

            exit;
        }
    }else {
        echo 'Invalid Token';
        die();
    }
}else {
    echo 'Invalid Parameter';
    die();
}
?>
