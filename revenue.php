<?php
$servername = "202.52.146.100";
$username = "iotsraco_test";
$password = "iotsra20200201";
$dbname = "iotsraco_summary";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//Authentication
if (isset($_GET['token_access'])) {
    $date_now = date('Y-m-d H:i:s');

    $sql_auth = 'SELECT * FROM token_access WHERE token = "'.$_GET['token_access'].'" AND expired_date > "'.$date_now.'"';
    $result_auth = $conn->query($sql_auth);

    if ($result_auth->num_rows == 0) {
        $data = [
            'status' => 2,
            'error' => 'Token Invalid. Please, Restart /start Command'
        ];

        echo json_encode($data);
        die();
    }
}else {
    $data = [
        'status' => 2,
        'error' => 'Token Invalid. Please, Restart /start Command'
    ];

    echo json_encode($data);
    die();
}

if (isset($_GET['date'])) {
    $billing_cycle = date('M Y', strtotime($_GET['date']));

    $sql = 'SELECT SUM(total_charge) as total_charge FROM revenue WHERE billing_cycle = "'.$billing_cycle.'"';
    $result = $conn->query($sql);

    while ($row = $result->fetch_assoc()) {
        $data_revenue = 'Total Revenue at '.$billing_cycle.' : '.number_format($row['total_charge'],0,'.',',');
    }

    $data = [
        'status' => 1,
        'success' => $data_revenue
    ];

    echo json_encode($data);
    die();

}else {
    $data = [
        'status' => 0,
        'error' => 'Please, enter billing cycle'
    ];

    echo json_encode($data);
    die();
}
?>
