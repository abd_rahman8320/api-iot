<?php
$servername = "202.52.146.100";
$username = "iotsraco_test";
$password = "iotsra20200201";
$dbname = "iotsraco_summary";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//Authentication
if (isset($_GET['token_access'])) {
    $date_now = date('Y-m-d H:i:s');

    $sql_auth = 'SELECT * FROM token_access WHERE token = "'.$_GET['token_access'].'" AND expired_date > "'.$date_now.'"';
    $result_auth = $conn->query($sql_auth);

    if ($result_auth->num_rows == 0) {
        $data = [
            'status' => 2,
            'error' => 'Token Invalid. Please, Restart /start Command'
        ];

        echo json_encode($data);
        die();
    }
}else {
    $data = [
        'status' => 2,
        'error' => 'Token Invalid. Please, Restart /start Command'
    ];

    echo json_encode($data);
    die();
}

if (isset($_GET['date'])) {
    $date = date('Y-m-d', strtotime($_GET['date']));

    $sql = 'SELECT iot_sales_engineer, SUM(total_price) as total FROM grace_period where DATE(updated) = "'.$date.'" GROUP BY iot_sales_engineer';
    $result = $conn->query($sql);

    $data_hasil = '';
    $total = 0;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $total += $row['total'];
            $data_hasil .= $row['iot_sales_engineer'].' : '.number_format($row['total'],0,'.',',').PHP_EOL;
        }

        $data_hasil .= 'Total GP '.$date.' : '.number_format($total,0,'.',',');

        $data = [
            'status' => 1,
            'success' => $data_hasil
        ];

        echo json_encode($data);
        die();
    }else {
        $data = [
            'status' => 0,
            'error' => 'There is no data for that date'
        ];

        echo json_encode($data);
        die();
    }
}else {
    $data = [
        'status' => 0, //Failed
        'error' => 'Please enter date'
    ];

    echo json_encode($data);
    die();
}
?>
