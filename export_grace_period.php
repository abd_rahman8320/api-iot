<?php
$servername = "202.52.146.100";
$username = "iotsraco_test";
$password = "iotsra20200201";
$dbname = "iotsraco_summary";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//Authentication
if (isset($_GET['token_access'])) {
    $date_now = date('Y-m-d H:i:s');

    $sql_auth = 'SELECT * FROM token_access WHERE token = "'.$_GET['token_access'].'" AND expired_date > "'.$date_now.'"';
    $result_auth = $conn->query($sql_auth);

    if ($result_auth->num_rows == 0) {
        $data = [
            'status' => 2,
            'error' => 'Token Invalid. Please, Restart /start Command'
        ];

        echo json_encode($data);
        die();
    }
}else {
    $data = [
        'status' => 2,
        'error' => 'Token Invalid. Please, Restart /start Command'
    ];

    echo json_encode($data);
    die();
}

$token = generate_token();

if (isset($_GET['date']) && isset($_GET['sales'])) {
    $date = date('Y-m-d', strtotime($_GET['date']));
    $sales = $_GET['sales'];
    $eru = explode('/', $_SERVER['REQUEST_URI']);
    $filename = 'file/grace_period/Grace Period '.$sales.' '.$date.'.csv';
    $expired_date = date('Y-m-d H:i:s', strtotime('+5 minutes'));

    if (file_exists($filename)) {
        $data = [
            'status' => 1,
            'success' => 'http://'.$_SERVER['SERVER_NAME'].'/'.$eru[1].'/export.php?q='.$token
        ];

        $sql2 = 'INSERT INTO token_download (token, file, expired_date) VALUES ("'.$token.'", "'.$filename.'", "'.$expired_date.'")';
        $insert = $conn->query($sql2);

        echo json_encode($data);
        die();
    }

    $columns = ['updated','account_name','iot_sales_engineer','operator_account_id','account_id','total_msisdn','harga_msisdn','total_price'];

    $sql = 'SELECT '.implode(',', $columns).' FROM grace_period where DATE(updated) = "'.$date.'" AND iot_sales_engineer LIKE "%'.$sales.'%"';
    $result = $conn->query($sql);

    $data_hasil = '';
    if ($result->num_rows > 0) {
        $file = fopen($filename, 'w');

        fputcsv($file, $columns);
        while ($row = $result->fetch_assoc()) {
            fputcsv($file, $row);
        }

        fclose($file);

        $sql2 = 'INSERT INTO token_download (token, file, expired_date) VALUES ("'.$token.'", "'.$filename.'", "'.$expired_date.'")';
        $insert = $conn->query($sql2);

        $data = [
            'status' => 1,
            'success' => 'http://'.$_SERVER['SERVER_NAME'].'/'.$eru[1].'/export.php?q='.$token
        ];

        echo json_encode($data);
        die();
    }else {
        $data = [
            'status' => 0,
            'error' => '0 Result'
        ];

        echo json_encode($data);
        die();
    }
}else {
    $data = [
        'status' => 0, //Failed
        'error' => 'Please enter date and sales name'
    ];

    echo json_encode($data);
    die();
}

function generate_token()
{
    $servername = "202.52.146.100";
    $username = "iotsraco_test";
    $password = "iotsra20200201";
    $dbname = "iotsraco_summary";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $token = md5(rand());

    $sql = 'SELECT * FROM token_download WHERE token = "'.$token.'"';
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $token = generate_token();

        return $token;
    }else {
        return $token;
    }
}
?>
